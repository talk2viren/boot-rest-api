package com.example.bootrestapo.entiry;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StudentEntity {
    private long studentId;
    private String firstName;
    private String lastName;
    private String city;
}
